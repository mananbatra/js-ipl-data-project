const fs = require('fs');
const csvFilePath = '../data/matches.csv'
const csv = require('csvtojson')

async function matchesWonPerTeamPerYear() {

    const matches = await csv().fromFile(csvFilePath);
    //console.log(matches);

    let yearOfIPL={};
    let matchesWonPerTeam={};
    //let matcheWon;

    for (let value of matches) {
        const { winner, season } = value;
      
        if (!(season in yearOfIPL)) {
          yearOfIPL[season] = {};
        }
      
        if (!(winner in yearOfIPL[season])) {
          yearOfIPL[season][winner] = 0;
        }
      
        yearOfIPL[season][winner]++;
    }

    //console.log(yearOfIPL);
    return yearOfIPL;

}

matchesWonPerTeamPerYear().then(data=>{

    const jsonData = JSON.stringify(data, null, 2);   // Convert object to JSON string with indentation

fs.writeFile('../public/output/2-matches-won-per-team-per-year.json', jsonData, 'utf8',(err) => {
  if (err) {
    console.error('Error writing JSON file:', err);
  } else {
    console.log('JSON file has been saved!');
  }
});

    //console.log(data)
});