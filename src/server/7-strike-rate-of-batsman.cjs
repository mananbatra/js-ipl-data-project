const fs = require('fs');
const matchFile = '../data/matches.csv'
const deliveriesFile = '../data/deliveries.csv'
const csv = require('csvtojson')

async function strikeRateOfBatsman() {

    const matches = await csv().fromFile(matchFile);
    const deliveries = await csv().fromFile(deliveriesFile);
    let yearOfIPL={};
    //let matchPlay={};

    //let batsmanData=deliveries.reduce((result, delivery));
    for(let match of matches)
    {
        for(let delivery of deliveries)
        {
            if(match.id===delivery.match_id)
            {
                //console.log(delivery.match_id)
                if(match.season in yearOfIPL)
                {
                    if (yearOfIPL[match.season][delivery.batsman]) {

                        yearOfIPL[match.season][delivery.batsman]['runs'] += parseInt(delivery.batsman_runs)

                        yearOfIPL[match.season][delivery.batsman]['balls'] += 1
                    } else {
                        yearOfIPL[match.season][delivery.batsman] = {}
                        yearOfIPL[match.season][delivery.batsman]['runs'] = parseInt(delivery.batsman_runs)
                        // if(delivery.extra_runs=='0'){
                        yearOfIPL[match.season][delivery.batsman]['balls'] = 1
                    }

                }else{
                    yearOfIPL[match.season] = {}
                    yearOfIPL[match.season][delivery.batsman] = {}
                    yearOfIPL[match.season][delivery.batsman]['runs'] = parseInt(delivery.batsman_runs)
                        // if(delivery.extra_runs=='0'){
                    yearOfIPL[match.season][delivery.batsman]['balls'] = 1

                }
            }

        }
    }
    let strikeRate={};
for(year in yearOfIPL){
    strikeRate[year]={};
    //console.log(year);
    for(batsman in yearOfIPL[year])
    {
        if(batsman==='V Kohli'){

            const runs = yearOfIPL[year][batsman].runs;
        const balls = yearOfIPL[year][batsman].balls;
        const playerStrikeRate = (runs / balls) * 100;
        strikeRate[year][batsman] = playerStrikeRate.toFixed(2);
         
        }
    }
}
      return strikeRate;   
}

strikeRateOfBatsman().then(data=>{

    const jsonData = JSON.stringify(data, null, 2);   // Convert object to JSON string with indentation

fs.writeFile('../public/output/7-strike-rate-of-batsman.json', jsonData, 'utf8',(err) => {
  if (err) {
    console.error('Error writing JSON file:', err);
  } else {
    console.log('JSON file has been saved!');
  }
});

    //console.log(data)
});

