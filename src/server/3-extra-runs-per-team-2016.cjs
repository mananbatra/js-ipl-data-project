const fs = require('fs');
const matchFile = '../data/matches.csv'
const deliveriesFile = '../data/deliveries.csv'
const csv = require('csvtojson')

async function extraRunPerTeam2016() {

    const matches = await csv().fromFile(matchFile);
    const deliveries = await csv().fromFile(deliveriesFile);
    let matchIdArray=[];

    for (let index in matches) {
        if (matches[index].season === '2016') {
            //console.log(matches[index].id);
            matchIdArray.push(matches[index].id)
        }
    }
    //console.log(matchIdArray);
    let extraRun=0;
    let extraRunPerTeam={};

    for(let value of matchIdArray)
    {
        for(let index in deliveries)
        {
            if(deliveries[index].match_id===value)
            {
                if(deliveries[index].bowling_team in extraRunPerTeam)
                {
                   extraRunPerTeam[deliveries[index].bowling_team]+= Number(deliveries[index].extra_runs);
                }
                else{
                    extraRunPerTeam[deliveries[index].bowling_team] = Number(deliveries[index].extra_runs);
                }
            }
        }
    }

    //console.log(extraRunPerTeam);
    return extraRunPerTeam;
}
extraRunPerTeam2016().then(data=>{

    const jsonData = JSON.stringify(data, null, 2);   // Convert object to JSON string with indentation

fs.writeFile('../public/output/3-extra-runs-per-team-2016.json', jsonData, 'utf8',(err) => {
  if (err) {
    console.error('Error writing JSON file:', err);
  } else {
    console.log('JSON file has been saved!');
  }
});

    //console.log(data)
});