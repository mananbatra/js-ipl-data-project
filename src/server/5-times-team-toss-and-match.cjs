const fs = require('fs');
const matchFile = '../data/matches.csv'
const csv = require('csvtojson')

async function teamWonTossAndMatch() {

    const matches = await csv().fromFile(matchFile);
    
    teamWonToss={};

    for(let index of matches)           // values of array matches
    {
        if(index.toss_winner in teamWonToss && index.toss_winner===index.winner)
        {
            //if(index.toss_winner===index.winner)   // if team present in object teamWonToss
            teamWonToss[index.toss_winner]+=1;
        }
        else if(index.toss_winner===index.winner)
        {
            teamWonToss[index.toss_winner]=1;    
        }
    }


  //console.log(teamWonToss);
  return teamWonToss;
}
teamWonTossAndMatch().then(data=>{

    const jsonData = JSON.stringify(data, null, 2);   // Convert object to JSON string with indentation

fs.writeFile('../public/output/5-times-team-toss-and-match.json', jsonData, 'utf8',(err) => {
  if (err) {
    console.error('Error writing JSON file:', err);
  } else {
    console.log('JSON file has been saved!');
  }
});

    //console.log(data)
});