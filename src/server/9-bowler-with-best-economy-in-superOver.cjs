const fs = require("fs");
const csvtojson = require("csvtojson");
const csvFilePath = "../data/deliveries.csv";


async function bowlerWithBestEconomy(csvFilePath) {
    const deliveries = await csvtojson().fromFile(csvFilePath);
    // console.log(data);
    const bowlers = {};
    let data = deliveries.filter((item) => {
        return item.is_super_over !== "0";
    });
    // console.log(data);
    for (let name of data) {
        if (name.bowler in bowlers) {
            bowlers[name.bowler]['runs'] += Number(name.total_runs);
            bowlers[name.bowler]['balls'] += 1;
        } else {
            bowlers[name.bowler] = {};
            bowlers[name.bowler]['runs'] = Number(name.total_runs);
            bowlers[name.bowler]['balls'] = 1;
        }
    }

    //console.log(bowlers);
    let economyCal = {};
    for (let bowler in bowlers) {
        economyCal[bowler] = {};
        //console.log(bowler);
        const runs = bowlers[bowler].runs;
        const balls = bowlers[bowler].balls;
        //console.log(runs + " ---------------" + balls);
        const playerEconomy = (runs / balls) * 6;
        economyCal[bowler] = playerEconomy.toFixed(2);
    }
    bestEconomy = []
    for (const bowler in economyCal) {
        const economy = economyCal[bowler];
        bestEconomy.push({ bowler, economy });
    }
    //console.log(bestEconomy);

    bestEconomy.sort((a, b) => a.economy - b.economy);
    const topEconomy = bestEconomy.slice(0, 1);
    //console.log(topEconomy);

    return JSON.stringify(topEconomy);
}

bowlerWithBestEconomy(csvFilePath).then((data) => {
    fs.writeFile("../public/output/9-bowler-with-best-economy-in-superOver.json", data, "utf8", (err) => {
        if (err) {
            console.error("An error occurred ", err);
        } else {
            console.log("Data store in Json");
        }
    });
});