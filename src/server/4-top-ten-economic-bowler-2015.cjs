const fs = require('fs');
const matchFile = '../data/matches.csv'
const deliveriesFile = '../data/deliveries.csv'
const csv = require('csvtojson')

async function topTenEconomicBowler() {

    const matches = await csv().fromFile(matchFile);
    const deliveries = await csv().fromFile(deliveriesFile);
    let matchIdArray=[];

    for (let index in matches) {
        if (matches[index].season === '2015') {
            //console.log(matches[index].id);
            matchIdArray.push(matches[index].id)
        }
    }
    //console.log(matchIdArray);
    let bowlerRuns={};
    let bowlerBalls={};

    for (let index of deliveries)
    {
        for(let value of matchIdArray)
        {
            if(index.match_id===value)
            {
                if(index.bowler in bowlerRuns)
                {
                    bowlerRuns[index.bowler]+=Number(index.total_runs);
                    bowlerBalls[index.bowler]+=1;
                }else{
                    bowlerRuns[index.bowler]=Number(index.total_runs);
                    bowlerBalls[index.bowler]=1;
                }
            }
        }
    }
    //console.log(bowlerRuns);
    //console.log(bowlerBalls);
    let bowlerEconomy={};
    let economy=0.000;
    //console.log(Object.keys(bowlerBalls));

    function economyCal(balls,runs)
    {
        for(let key in balls)
        {
            if(key in runs)
            {
                bowlerEconomy[key]=runs[key]*6/balls[key];
            }
        }
        //console.log(bowlerEconomy);
        return bowlerEconomy;
    }
    economyCal(bowlerBalls,bowlerRuns);
    //console.log(bowlerEconomy);

    const sortedEconomy = Object.fromEntries(
        Object.entries(bowlerEconomy).sort(([, value1], [, value2]) => value1 - value2));
    
    const top10Economy = Object.fromEntries(
            Object.entries(sortedEconomy).slice(0,10));
      //console.log(top10Economy);

      return top10Economy;
}

topTenEconomicBowler().then(data=>{

    const jsonData = JSON.stringify(data, null, 2);   // Convert object to JSON string with indentation

fs.writeFile('../public/output/4-top-ten-economic-bowler-2015.json', jsonData, 'utf8',(err) => {
  if (err) {
    console.error('Error writing JSON file:', err);
  } else {
    console.log('JSON file has been saved!');
  }
});

    //console.log(data)
});