const fs = require('fs');
const csvFilePath = '../data/matches.csv'
const csv = require('csvtojson')

async function mostManOfMatchPerYear() {

    const matches = await csv().fromFile(csvFilePath);
    //console.log(matches);

    let yearOfIPL={};
    //let matchesWonPerTeam={};
    //let matcheWon;

    for (let value of matches) {
        const { player_of_match, season } = value;
      
        if (!(season in yearOfIPL)) {
          yearOfIPL[season] = {};
        }
      
        if (!(player_of_match in yearOfIPL[season])) {
          yearOfIPL[season][player_of_match] = 0;
        }
      
        yearOfIPL[season][player_of_match]++;
    }

    //const sortedObj={};
    for(let key in yearOfIPL)
    {   
        let entries=Object.entries(yearOfIPL[key]);
        //console.log(entries);
        let array=entries.sort((a, b) => b[1] - a[1]);

        yearOfIPL[key]=array[0][0];
    }
    //console.log(yearOfIPL);
    return yearOfIPL;

}

mostManOfMatchPerYear().then(data=>{

    const jsonData = JSON.stringify(data, null, 2);   // Convert object to JSON string with indentation

fs.writeFile('../public/output/6-player-with-highest-MOM-award.json', jsonData, 'utf8',(err) => {
  if (err) {
    console.error('Error writing JSON file:', err);
  } else {
    console.log('JSON file has been saved!');
  }
});

    //console.log(data)
});