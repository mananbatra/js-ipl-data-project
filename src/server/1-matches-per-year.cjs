const fs = require('fs');
const csvFilePath = '../data/matches.csv'
const csv = require('csvtojson')

async function matchesPerYear() {

    const matches = await csv().fromFile(csvFilePath);
    //console.log(matches);
    let yearOfIPL={} 
    let numberOfMatches; //to calculate matches per year


    for (let index in matches)
    {
       let matchObj=matches[index];             // loop through every value of array matches
       //console.log(matchObj.season)
        if(matchObj.season in yearOfIPL)        // if year already present 
        {
            yearOfIPL[matchObj.season] += 1;
        }else                                   // if year not present 
        {
            yearOfIPL[matchObj.season] = 1;
        }
    }
    
        //console.log(yearOfIPL);
        return yearOfIPL;
}

matchesPerYear().then(data=>{

    const jsonData = JSON.stringify(data, null, 2);   // Convert object to JSON string with indentation

fs.writeFile('../public/output/1-matches-per-year.json', jsonData, 'utf8',(err) => {
  if (err) {
    console.error('Error writing JSON file:', err);
  } else {
    console.log('JSON file has been saved!');
  }
});

    //console.log(data)
});

//console.log(result);


