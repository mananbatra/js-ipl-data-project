const fs = require('fs');
const deliveriesFile = '../data/deliveries.csv'
const csv = require('csvtojson')

async function highestTimePlayerDismissAnotherPlayer() {

    const deliveries = await csv().fromFile(deliveriesFile);

    let bowlerDismissals={}

    for(delivery of deliveries)
    {
        //console.log(delivery.dismissal_kind);
        if(delivery.dismissal_kind !=='run out' && delivery.dismissal_kind !=='' && delivery.dismissal_kind !=='hit wicket' && delivery.dismissal_kind !=='retired hurt')
        {
            if(delivery.bowler in bowlerDismissals)
            {
                if (bowlerDismissals[delivery.bowler][delivery.player_dismissed]) {
                    
                    bowlerDismissals[delivery.bowler][delivery.player_dismissed]['dismissals'] += 1
                } else {
                    bowlerDismissals[delivery.bowler][delivery.player_dismissed] = {}
                    
                    bowlerDismissals[delivery.bowler][delivery.player_dismissed]['dismissals'] = 1
                }
            }else{
                bowlerDismissals[delivery.bowler] = {}
                bowlerDismissals[delivery.bowler][delivery.player_dismissed] = {}
                bowlerDismissals[delivery.bowler][delivery.player_dismissed]['dismissals'] = 1
            }
        }
    }
    
    const highestDismissal = [];
for (const bowler in bowlerDismissals) {
  for (const batsman in bowlerDismissals[bowler]) {
    const dismissals = bowlerDismissals[bowler][batsman].dismissals;
    highestDismissal.push({ bowler, batsman, dismissals });
  }
}
highestDismissal.sort((a, b) => b.dismissals - a.dismissals);
const topDismissals = highestDismissal.slice(0, 1);

   return topDismissals;
}

highestTimePlayerDismissAnotherPlayer().then(data=>{

    const jsonData = JSON.stringify(data, null, 2);   // Convert object to JSON string with indentation

fs.writeFile('../public/output/8-highest-number-of-times-batsman-dismissed-by-perticular-bowler.json', jsonData, 'utf8',(err) => {
  if (err) {
    console.error('Error writing JSON file:', err);
  } else {
    console.log('JSON file has been saved!');
  }
});

    //console.log(data)
});
